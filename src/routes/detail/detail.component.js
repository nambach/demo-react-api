import React from 'react';
import _ from 'lodash';

const DEFAULT_ID = 1;

const UserView = ({
  name,
  email,
  address: { street, city } = {},
  phone,
  handleEdit
}) => (
  <div>
    <h3>{name}</h3>
    <p><i>{email}</i></p>
    <p><span>{street}, {city}</span></p>
    <p><span>{phone}</span></p>
    <button onClick={handleEdit}>EDIT</button>
  </div>
);

const UserEdit = ({
  name,
  email,
  address: { street, city } = {},
  phone,
  handleInputChange,
  handleCancel,
  handleSubmit
}) => (
  <div>
    <form onSubmit={handleSubmit}>
      <input type="text" value={name} onChange={e => handleInputChange('name', e.target.value)} /><br/>
      <input type="text" value={email} onChange={e => handleInputChange('email', e.target.value)} /><br/>
      <input type="text" value={street} onChange={e => handleInputChange('address.street', e.target.value)} />
      <input type="text" value={city} onChange={e => handleInputChange('address.city', e.target.value)} /><br/>
      <input type="text" value={phone} onChange={e => handleInputChange('phone', e.target.value)} /><br/>
      <button>SAVE</button>
      <button onClick={handleCancel}>CANCEL</button>
    </form>
  </div>
);

export default class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      isEditing: false
    };
  }

  componentDidMount() {
    const id = _.get(this.props, 'match.params.id', DEFAULT_ID);

    fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
      .then(res => res.json())
      .then(data => {
        this.setState({ user: data });
      });
  }

  handleEdittingMode = status => {
    this.setState({ isEditing: status });
  }

  handleInputChange = (field, value) => {
    const { user } = this.state;
    const newUser = {
      ...user,
      address: { ...user.address }
    };

    this.setState({ user: _.set(newUser, field, value) });
  }

  handleSubmit = e => {
    e.preventDefault();
    const id = _.get(this.state, 'user.id', null);

    if (id) {
      fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
        method: 'PUT',
        body: JSON.stringify(this.state.user)
      })
        .then(res => res.json())
        .then(() => {
          this.handleEdittingMode(false);
          // eslint-disable-next-line
          const confirmResult = confirm("Update successfully!");

          if (confirmResult) {
            const push = _.get(this.props, 'history.push', () => {});

            push({ pathname: '/' });
          }
        });
    }
  }

  render() {
    const { isEditing, user } = this.state;

    return isEditing
      ? <UserEdit
          {...user}
          handleCancel={() => this.handleEdittingMode(false)}
          handleSubmit={this.handleSubmit}
          handleInputChange={this.handleInputChange}
        />
      : <UserView
          {...user}
          handleEdit={() => this.handleEdittingMode(true)}
        />;
  }
}
