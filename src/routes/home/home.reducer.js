import { SET_USERS } from './home.constants';

const initialState = {
  users: []
};

const setUsers = (state, action) => {
  const { users } = action;

  return {
    ...state,
    users
  };
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USERS:
      return setUsers(state, action);
    default:
      return state;
  }
};

export default homeReducer;
