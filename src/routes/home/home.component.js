import React from 'react';
import { Table, Pagination } from 'react-bootstrap';

// import User from './components/user';
import Row from './components/row';

const FIRST_PAGE = 1;
const SECOND_PAGE = 2;

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: FIRST_PAGE
    };
  }

  componentDidMount() {
    this.loadPage(FIRST_PAGE);
  }

  loadPage = pageNumber => {
    fetch(`https://jsonplaceholder.typicode.com/users?_page=${pageNumber}&_limit=5`)
      .then(res => res.json())
      .then(data => {
        this.props.setUsers(data);
        this.setState({ page: pageNumber });
      });
  }

  render() {
    const { users } = this.props;
    const { page } = this.state;

    return (
      <div>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Address</th>
              <th>Phone Number</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {users.map(user => (
              <Row key={user.id} {...user} />
            ))}
          </tbody>
        </Table>
        <Pagination>
          <Pagination.Item
            onClick={() => this.loadPage(FIRST_PAGE)}
            active={page === FIRST_PAGE}
          >
            1
          </Pagination.Item>
          <Pagination.Item
            onClick={() => this.loadPage(SECOND_PAGE)}
            active={page === SECOND_PAGE}
          >
            2
          </Pagination.Item>
        </Pagination>
      </div>
    );
  }
}
