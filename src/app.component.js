import React from 'react';
import { Switch, Route, Link } from "react-router-dom";
import { Navbar, Nav, Container } from 'react-bootstrap';

import Home from './routes/home';
import Detail from './routes/detail';
import Settings from './routes/settings';

import './app.style.scss';

class App extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link><Link to="/">Home</Link></Nav.Link>
              <Nav.Link><Link to="/detail">Detail</Link></Nav.Link>
              <Nav.Link><Link to="/settings">Settings</Link></Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Container>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/detail/:id" component={Detail} />
            <Route path="/settings/" component={Settings} />
          </Switch>
        </Container>
      </React.Fragment>
    );
  }
}

export default App;
